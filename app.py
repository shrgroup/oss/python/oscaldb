import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output, State, MATCH, ALL
from flask import Flask
from pyoscal.core import OSCAL
import base64, pickle
import lxml.etree as etree
from bson.json_util import dumps as bson_dumps

from pymongo import MongoClient
client = MongoClient('localhost', 27017)
db = client['oscaldb']
coll = db.objects

server = Flask('oscaldb')
app = dash.Dash(__name__, server=server, external_stylesheets=[dbc.themes.BOOTSTRAP])

#objects = []
def get_objects():
    return coll.find({})
def add_object(obj):
    coll.update({'uuid': obj.get('uuid')}, obj, upsert=True);
def get_uuid(uuid):
    return coll.find_one({'uuid': uuid})

@server.route("/<string:uuid>")
def download(uuid):
    obj = get_uuid(uuid)
    return bson_dumps(obj)

def obj_card(obj):
    id = obj['uuid']
    title = obj['name']

    ## THIS MADNESS IS FOR PRETTY PRINTING ##
    try:
        xml = base64.decodebytes(obj['raw'])
        parser = etree.XMLParser(ns_clean=True, recover=True, encoding='utf-8', remove_blank_text=True)
        xml = etree.fromstring(xml, parser=parser)
        xml = etree.tostring(xml, pretty_print=True).decode('utf-8')
    except:
        xml = base64.decodebytes(obj['raw'])
        xml = xml.decode('utf-8')

    return dbc.Col(dbc.Card([
        dbc.CardHeader(f"{id}"),
        dbc.CardBody(f"{title}"),
        dbc.Button(
            f"View",
            id={'name': f"{id}", 'type': "obj-toggle" },
            color="primary",
            n_clicks=0,
        ),
        dbc.Modal([
            dbc.ModalHeader(f"{id}: {title}"),
            dbc.ModalBody([
                html.Pre(f"{xml}"),
            ])
        ],id={'name': f"{id}", 'type': "obj-view" }, style={"max-width": "none", "width": "90%"}, is_open=False)
    ]),width="auto",style={'width': '300px', 'display': 'inline-block', 'overflow': 'auto'})


def obj_group(group, objs):
    return html.Div([
        dbc.Button(
            f"{group}s",
            id={
                'name': f"{group}",
                'type': "group-hide"
            },
            color="primary",
            n_clicks=0,
        ),
        dbc.Collapse(
            dbc.Row(
                html.Div([obj_card(obj) for obj in objs],
                    id={'name': f"{group}",'type': "group-div"},
                    style={'display': 'inline-block', 'height': '250px', 'width':'100%', 'overflow-x': 'auto', 'overflow-y': 'hidden'}
                )
            ),
            id={
                'name': f"{group}",
                'type': "group-collapse"
            },
            is_open=False,
        )
    ], style={'width': '100%'})
    
def display_objects():
    groups = {}
    for obj in get_objects():
        obj_type = obj['obj_type']
        if obj_type not in groups:
            groups[obj_type] = []
        groups[obj_type] += [obj]
    return html.Div([obj_group(group, groups[group]) for group in groups])


@app.callback(
    Output({'type': 'obj-view', 'name': MATCH}, "is_open"),
    [Input({'type': 'obj-toggle', 'name': MATCH}, "n_clicks")],
    [State({'type': 'obj-view', 'name': MATCH}, "is_open")],
    prevent_initial_call=True,
)
def toggle_object(n, is_open):
    if n:
        return not is_open
    return is_open

@app.callback(
    Output({'type': 'group-collapse', 'name': MATCH}, "is_open"),
    [Input({'type': 'group-hide', 'name': MATCH}, "n_clicks")],
    [State({'type': 'group-collapse', 'name': MATCH}, "is_open")],
    prevent_initial_call=True,
)
def toggle_collapse(n, is_open):

    if n:
        openclose = not is_open
    else:
        openclose = is_open
    return openclose

@app.callback(
    Output("display_objects", "children"),
    [Input("upload-data", "filename"), Input("upload-data", "contents")],
)
def parse_and_save(uploaded_filenames, uploaded_file_contents):
    if uploaded_filenames is not None and uploaded_file_contents is not None:
        for name, data in zip(uploaded_filenames, uploaded_file_contents):
            data = data.encode("utf8").split(b";base64,")[1]
            string = base64.decodebytes(data)
            oscal_data = OSCAL().parse_string(string)

            uuid = oscal_data.uuid.prose
            try:
                name = str(oscal_data.metadata.title)
            except Exception:
                name = str(oscal_data)
            obj = dict(
                obj_type=oscal_data.__class__.__name__,
                filename=name,
                uuid=uuid,
                name=name,
                raw=data,
                data=pickle.dumps(oscal_data)
            )
            add_object(obj)

    return display_objects()

app.layout = html.Div([
    html.H1(f"OSCAL Registry", style={'textAlign': 'center'}),
    dcc.Upload([
            'Drag and Drop or ',
            html.A('Select a File')
            ], style={
                'width': '100%','height': '60px','lineHeight': '60px','borderWidth': '1px',
                'borderStyle': 'dashed','borderRadius': '5px','textAlign': 'center'
        },id='upload-data',multiple=True
    ),
    html.Div([],id='display_objects')
])

if __name__ == '__main__':
    app.run_server(host='0.0.0.0', port='5001', debug=True)