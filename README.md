# oscalDB

Repository/Registry of OSCAL content parsed by using pyOSCAL library.

## Name
OSCALDB is a python backed database of pre-parsed objectified OSCAL files.  

## Description
The goal will be to allow front-end tools to use a shared database fetch pre-defined reference objects by name, uuid, or type.


```plantuml
    actor "User" as user

    file "Oscal (xml,json,yml)" as oscal_data
    file "Oscal (xml,json,yml)" as oscal_data_out
    file "pyOscal 1010101" as pyoscal_data
    card "UUID" as uuid_request

    component "OscalDB" as oscaldb
    database "Backend" as backend

    user --> oscal_data
    user --> uuid_request
             oscal_data   --> oscaldb : Parse & Store
             uuid_request --> oscaldb : fetch
                              oscaldb --> oscal_data_out : Shareable Model
                              oscaldb --> pyoscal_data : pickle file of python object
    oscaldb <-> backend

```

## Story

1. User uploads OSCAL xml file.
2. file is parsed by pyoscal
3. metadata (uuids, title)
4. metadata, original _raw text, and pickled binary data stored in MongoDB
5. Dash frontend provides upload and download

## Installation / Usage
Run Docker Container.  connect /data to persistent Volume

### Mongo
`docker run --name oscaldb -v ${PWD}/data:/data/db -p 27017:27017 mongo`

## The GUI

![Video of user uploading files to oscaldb](/img/upload.gif "Upload Files")

![Video of user viewing the raw XML of an uploaded file](/img/view_xml.gif "View XML")
## Contributing

* Create Issues
* Fork and make Pull Requests

## Authors and acknowledgment
Started and currently maintained by SHRgroup LLC's Emerging Technology Directorate 

## License
Apache 2.0

## Project status

Protype / Initial Planning

